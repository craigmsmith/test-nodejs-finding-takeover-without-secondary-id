import createAndStartServer from "./server.js"
import sampleRoute from "./src/sample/sampleRoute.js"
import xssRoute from "./src/xss/xss-serialize-javascript.js"
import xssHandlebarsNoescapeRoute from "./src/xss/xss-handlebars-noescape.js"
import xssHandlebarsRoute from "./src/xss/xss_handlebars_safe_string.js"
import homeRoute from "./src/HomeRoute.js"
import xssExpressRoute from "./src/xss/xss-express.js"
import squirrellyRoute from "./src/xss/xss-squirrelly-autoescape.js"

const routers = {
    "/": homeRoute,
    "/sample": sampleRoute,
    "/xss/serialize": xssRoute,
    "/xss/squirrelly": squirrellyRoute,
    "/xss/express": xssExpressRoute,
    "/xss/handlebars/safe-string": xssHandlebarsRoute,
    "/xss/handlebars/noescape": xssHandlebarsNoescapeRoute,
}

createAndStartServer(routers)