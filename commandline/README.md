## commandline

Used for testing simple javascript sources/sinks that do not require web applications.


### Running

Run a single class:
```
docker build -t js-commandline-app . && docker run js-commandline-app node app.js TestMD5Hash
```

Run all classes:
```
docker build -t js-commandline-app . && docker run js-commandline-app node app.js all
```

List all classes:
```
docker build -t js-commandline-app . && docker run js-commandline-app node app.js list
```

### Rules

For Rule : rules_lgpl_javascript_crypto_rule-node-md5
Test Class : TestMD5Hash

For Rule : rules_lgpl_javascript_crypto_rule-node-insecure-random-generator
Test Class : TestInsecureRandomNumberGenerators