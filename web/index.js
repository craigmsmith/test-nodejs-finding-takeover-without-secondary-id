const express = require('express')
const path = require('path')
const app = express()
const port = 3000

app.use(express.urlencoded({ extended: true }))
app.use(express.json())

require('./mysql_injection')(app)
require('./pg_injection')(app)
require('./sequelize_injection')(app)
require('./eval_injection')(app)
require('./open_redirect2')(app)
require('./open_redirect1')(app)
require("./xss-serialize-javascript")(app);
require("./xss-handlebars-noescape")(app);
require("./src/xss/xss-express")(app);
require("./src/xss/xss_handlebars_safe_string")(app);
require("./xss-squirrelly-autoescape")(app);
require("./mustache_escape")(app);

// Serve static files from the 'public' directory
app.use(express.static(path.join(__dirname, 'public')))

app.listen(port, () => {
  console.log(`Express app listening at http://localhost:${port}`)
})

// app.get("/", (req, res) => {
//   res.send("Hello World!");
// });

app.get('/', (req, res) => {
  const links = [
    {
      url: '/mysql/one?username=Phil+Dunphy&nickname=philly',
      text: 'MySQL test 1'
    },
    { url: '/mysql/two?id=4', text: 'MySQL test 2' },
    {
      url: '/mysql/three?id=5',
      text: 'MySQL test 3'
    },
    { url: '/pg/one?id=4', text: 'Postgres test 1' },
    {
      url: '/pg/two?nickname=Lukey',
      text: 'Postgres test 2'
    },
    { url: '/seq/one?id=2', text: 'Sequelize test 1' },
    {
      url: '/seq/two?username=Phil+Dunphy&nickname=philly',
      text: 'Sequelize test 2'
    },
    {
      url: '/seq/three?username=Phil+Dunphy&nickname=philly',
      text: 'Sequelize test 3'
    },
    {
      url: '/seq/four?username=Phil+Dunphy&nickname=philly',
      text: 'Sequelize test 4'
    },
    { url: '/seq/five', text: 'Sequelize test 5' },
    {
      url: '/seq/six?username=Phil+Dunphy&nickname=philly',
      text: 'Sequelize test 6'
    },
    {
      url: '/seq/seven?username=Phil+Dunphy&nickname=philly',
      text: 'Sequelize test 7'
    },
    {
      url: "/seq/eight",
      text: "Sequelize test 8",
    },
    {
      url: "/xss/serialize/1?message=<script>alert('sd')</script>",
      text: "Xss-serialize-javascript unsafe",
    },
    {
      url: "/xss/serialize/safe/1?message=<script>alert('sd')</script>",
      text: "Xss-serialize-javascript safe",
    },
    {
      url: "/xss/serialize/safe/2?message=<script>alert('sd')</script>",
      text: "Xss-serialize-javascript safe",
    },
    {
      url: "/xss/squirrelly/1",
      text: "Squirrelly safe",
    },
    {
      url: "/xss/squirrelly/safe/1",
      text: "Squirrelly safe",
    },
    {
      url: "/xss/squirrelly/safe/2",
      text: "Squirrelly safe"
    },
    {
      url: "/xss/handlebars/safe-string/1?message=<script>alert(%27xss%27)</script>",
      text: "Xss-handlebars-safe-string",
    },
    {
      url: "/xss/handlebars/noescape/1?message=<script>alert('XSS')</script>",
      text: "Xss-Handlebars-noescape unsafe",
    },
    {
      url: "/xss/handlebars/noescape/safe/1?message=<script>alert('XSS')</script>",
      text: "Xss-Handlebars-noescape safe",
    },
    {
      url: "/xss/handlebars/noescape/safe/2?message=<script>alert('XSS')</script>",
      text: "Xss-Handlebars-noescape safe",
    }
    // Add more links as needed
  ]

  // Start with an HTML structure
  let htmlResponse = `
        <html>
            <head>
                <title>API Endpoints</title>
                <style>
                    body { font-family: Arial, sans-serif; }
                    ul { list-style-type: none; }
                    li { margin: 10px 0; }
                    a { text-decoration: none; color: blue; }
                    a:hover { text-decoration: underline; }
                </style>
            </head>
            <body>
            <div style="background-image: url(javascript:alert('XSS'))">
                <h1>Available API Endpoints</h1>
                <p> 
                Feel free to change values of query parameters.
                Check the Readme file to see the db entries. 
                P.S. The request queries might look similar, but the db queries behind are structured in different formats to test for varied patterns.
                </p>
                <ul>`

  // Add each link as a list item
  links.forEach(link => {
    htmlResponse += `<li><a href="${link.url}">${link.text}</a></li>`
  })

  // Close the HTML tags
  htmlResponse += `
                </ul>
                <h1>Tests for JavaScript Mustache Escape</h1>
                <p> 
                Please visit "http://localhost:3000/mustache-escape" for the tests or click the button below.
                </p>
                <button onclick="goToMustacheEscape()"> 
                    Go to Mustache Escape 
                </button> 
              
                <script> 
                    function goToMustacheEscape() { 
                        window.location.href =  
                            "http://localhost:3000/mustache-escape"; 
                    } 
                </script> 

                <br><br>
                <p> For Rule : 'https://gitlab.com/gitlab-org/security-products/sast-rules/-/blob/main/javascript/eval/rule-eval-with-expression.yml'
                    -> Navigate to <a href="/dangerous-sinks?name=Cameron">http://localhost:3000/dangerous-sinks?name=Cameron</a> </p>
                <p> For Rule : 'https://gitlab.com/gitlab-org/security-products/sast-rules/-/blob/main/rules/lgpl/javascript/redirect/rule-express_open_redirect.yml'
                    -> Navigate to <a href="/redirect">http://localhost:3000/redirect</a> </p>                
                <p> For Rule : 'https://gitlab.com/gitlab-org/security-products/sast-rules/-/blob/main/rules/lgpl/javascript/redirect/rule-express_open_redirect2.yml'
                    -> Navigate to <a href='/redirect-two'>http://localhost:3000/redirect-two</a></p>

            </body>
        </html>`

  // Send the response
  res.send(htmlResponse)
})
